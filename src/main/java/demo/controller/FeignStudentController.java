package demo.controller;

import demo.dto.StudentDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import demo.service.StudentService;

@RestController
@RequestMapping("/feignStudent")
@RequiredArgsConstructor
public class FeignStudentController {
   private final StudentService studentService;
@GetMapping("/id")
    public StudentDto getById(@RequestParam Long id){
        return studentService.getById(id);
    }

}
