package demo.service;

import demo.dto.StudentDto;
import org.springframework.stereotype.Service;

@Service
public interface StudentService {

    public StudentDto getById(Long id);
}
