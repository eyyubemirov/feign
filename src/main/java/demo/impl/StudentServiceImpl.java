package demo.impl;

import demo.dto.StudentDto;
import demo.feign.StudentClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import demo.service.StudentService;

@Service
@RequiredArgsConstructor

public class StudentServiceImpl  implements StudentService {
    private final StudentClient studentClient;


    @Override
    public StudentDto getById(Long id) {
        return studentClient.getByID(id);
    }
}
