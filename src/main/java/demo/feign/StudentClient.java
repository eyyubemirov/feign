package demo.feign;

import demo.dto.StudentDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "demo",url = "http://localhost:8080/student")
public interface StudentClient {
@GetMapping("/getById")
    public StudentDto getByID(@RequestParam Long id);
}
